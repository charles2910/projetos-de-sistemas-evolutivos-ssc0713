#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "ag.h"

result_simulacao * ag1(gene * inicial) {
	int max_gen = inicial->geracoes,
	    pop = inicial->pop_inicial;
	double mut = inicial->mutacao;
	result_simulacao *melhor_ag = NULL;


	// Inicialização dos 'agentes'
	double agentes[pop], resultados[pop], melhor = 0.00;

	for(int i = 0; i < pop; i++) {
		agentes[i] = (double) (rand() % (MAX_X * 100))/100.00;
	}

	int iter = 0, rank[pop];
	
	while(iter <= max_gen) {
		iter++;
		for(int i = 0; i < pop; i++) {
			resultados[i] = funcao(agentes[i]);
		}
		ranking(agentes, rank, inicial);

		if (iter == 1){
//			printf("|%lf| |%lf| \n", resultados[rank[0]], agentes[0]);
		}

		mut = altera_mutacao(agentes[rank[0]] - melhor, inicial->threshold/10.0, mut);
		
		//Atribue ao melhor se for realmente melhor
		if (funcao(melhor) < funcao(agentes[rank[0]])) {
			//if(resultados[rank[0]] - funcao(melhor) < 10e-6 ) break;
			melhor = agentes[rank[0]];
		}

		reproduz(agentes, rank, inicial);

		mutacao(agentes, rank, inicial);

		if ((max_gen > 100) & (iter % (int) (0.01 * max_gen) == 0)) {
			predacao (agentes, rank, inicial);
		}
		if (funcao(melhor) > 24.305676912) break;
	}
	melhor_ag = gera_resultado(iter, melhor);
	
	//printf("|x: %lf| y: %.9lf| \n", melhor,funcao(melhor));

	return melhor_ag;
}

/**
 * Realiza a reprodução de acordo com o método especificado
 */
void reproduz(double *agentes, int *ranking, gene *base) {
	int pop = base->pop_inicial;
	int metodo_reprod = base->metodo_selecao;

	if (metodo_reprod == 1) {
		reproducao(agentes, ranking, base);
	} else if (metodo_reprod == 2) {
		roleta(agentes, ranking, base);
	} else if (metodo_reprod == 3) {
		elitismo(agentes, ranking, base);
	}
}


/**
 * Verifica se deve-se aumentar ou diminuir a taxa de mutação
 */
double altera_mutacao(double diferenca, double limiar, double mutacao) {
	diferenca = fabs(diferenca);

	if(diferenca < limiar) {
		if(mutacao * 1.5 < 50) {
			mutacao = mutacao * 1.5;
		}
	} else {
		mutacao = mutacao / 3;
	}
	return mutacao;
}

/**
 * Função que aloca resultados
 */
result_simulacao * gera_resultado(int n_geracoes, double melhor_ag) {
	result_simulacao * ag = (result_simulacao *) malloc(sizeof(result_simulacao));

	ag->n_geracoes = n_geracoes;
	ag->melhor = melhor_ag;

	return ag;
}

/**
 * Função objetivo, método de avaliação, deve ser maximizada.
 */
double funcao(double x) {
	return 2*cos(0.39*x) + 5*sin(0.5*x) + 0.5*cos(0.1*x) + 10*sin(0.7*x) + 5*sin(1*x) + 5*sin(0.35*x);
}


/**
 * Função que retorna as posições dos melhores agentes. Isto é, o
 * primeiro valor corresponde à posição do melhor agente.
 */
int ranking(double *vetor, int *rank, gene *base) {
	struct dupla {
		double valor;
		int pos;
	};

	int pop = base->pop_inicial;

	if(vetor == NULL)
		return -1;

	struct dupla * auxiliar = (struct dupla *) calloc(pop, sizeof(struct dupla));
	for(int i = 0; i < pop; i++) {
		auxiliar[i].valor = funcao(vetor[i]);
		auxiliar[i].pos = i;
	}

	// sorting
	struct dupla aux;
	for(int i = 0; i < pop; i++) {
		for(int j = i + 1; j < pop; j++) {
			if (auxiliar[j].valor > auxiliar[i].valor) {
				aux = auxiliar[i];
				auxiliar[i] = auxiliar[j];
				auxiliar[j] = aux;
			}
		}
	}

	for(int i = 0; i < pop; i++) {
		//printf("Posição %d, valor %lf\n", auxiliar[i].pos, auxiliar[i].valor);
		rank[i] = auxiliar[i].pos;
	}
	free(auxiliar);
	return 0;
}

/**
 * Função que gera nova geração de agentes.
 */
int reproducao(double *agente, int *rank, gene *base) {
	int pop = base->pop_inicial;
	double * filhos = (double *) calloc(pop, sizeof(double));
	for (int i = 0; i < pop; i++)
		filhos[i] = (agente[rank[rand() % pop]] + agente[rank[rand() % pop]]) / 2.000;
	free(filhos);
	return 0;
}

void elitismo(double *agente, int *rank, gene *base){ //media do melhor com todos
	int pop = base->pop_inicial;
	double * filhos = (double *) calloc(pop, sizeof(double));
	for (int i = 0; i < pop; i++)
		filhos[i] = (agente[rank[0]] + agente[i]) / 2.000;

	for (int i = 0; i < pop; i++)
		agente[i] = filhos[i];
	free(filhos);
}

void roleta(double *agente, int *rank, gene *base){ //esquema de roleta com os 3 mlehores
	int pop = base->pop_inicial;
	double pri = 40.00, seg = 35.00, ter = 25.00;
	double r;
	double * filhos = (double *) calloc(pop, sizeof(double));

	for (int i = 0; i < pop; i++){
		r = (rand() % 10000)/100;//gera ujm double entre 0 e 100

		if(i != rank[0]){ //protege o melhor
			if(r > 0 && r <= pri){//o melhor
				filhos[i] = (agente[rank[0]] + agente[i]) / 2.000;

			}else if(r > pri && r <= (seg+pri)){//o segundo
				filhos[i] = (agente[rank[1]] + agente[i]) / 2.000;

			}else if(r > (seg+pri) && r <= (ter+seg+pri)){//o terceiro
				filhos[i] = (agente[rank[2]] + agente[i]) / 2.000;

			}else{//caso as probabilidades nao somem 100 da preferencia para o melhor
				filhos[i] = (agente[rank[0]] + agente[i]) / 2.000;
			}
		}else{
			filhos[i] = agente[i];
 		}
	}

	for (int i = 0; i < pop; i++)
		agente[i] = filhos[i];
	free(filhos);
}


void mutacao(double *agente, int *rank, gene *base){
	int pop = base->pop_inicial;
	double TaxMut = base->mutacao;
	for(int i=0; i<pop; i++){
		if(i != rank[0])
			agente[i] = agente[i] + (double) (((rand()%MAX_X - (MAX_X/2.0))/100.0) * TaxMut);
		if(agente[i] > MAX_X || agente[i] < 0)
			agente[i] = (double) (rand() % (MAX_X * 100))/100.00;
	}
}

void predacao (double* agentes, int* rank, gene *base){
	int pop = base->pop_inicial;
	int * array = (int *) calloc(pop, sizeof(int));
	int temp;
	double TaxPred = base->qtd_predacao;

	for(int i = 0; i<pop; i++){
		array[i] = 0;
	}

	for(int i = 0; i<TaxPred; i++){
		temp = rand()%pop;
		if (array[temp] == 0){
			agentes[temp] = (double) (rand() % (MAX_X * 100))/100.00;
			array[temp] = 1;
		}
	}
}
