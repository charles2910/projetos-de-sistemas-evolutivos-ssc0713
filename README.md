# AGente Duplo - Projeto de Sistemas Evolutivos 2020

#### Autores:
- Carlos Henrique Lima Melara - Numero USP: 9805280
- Guilherme Amaral Hiromoto - Numero USP: 11218959
- Paulo Matana da Rocha - Numero USP: 10892676


O projeto desenvolvido pelo grupo foi criar um algoritmo genético capaz de treinar outros algoritmos genéticos. Isto é, criar um algoritmo (Ag2) que seja capaz de definir os melhores parâmetros -  taxa de mutação, método de seleção, população inicial e quantidades de indivíduos removidos no evento de predação - para outro algoritmo genético (Ag1).


![](https://i.imgur.com/FvdkDk1.png)

![](https://i.imgur.com/1HIjmTi.jpg)


## Ag1 
#### Algoritmo genético evolutivo que será melhorado por Ag2
    
O algoritmo Ag1 é um algoritmo genético evolutivo que tem como objetivo encontrar o máximo global de uma função matemática 2d. 

Função utilizada:
<p style="text-align: center;"><b>2*cos(0.39*x)+5*sin(0.5*x)+0.5*cos(0.1*x)+10*sin(0.7*x)+5*sin(1*x)+5*sin(0.35*x)</b></p>

![](https://i.imgur.com/KRhlfBk.png)


O sistema de evolução de Ag1 utiliza várias técnicas para melhorar seu desempenho, como, a mutação variável, a predação de indivíduos e três tipos diferentes de reprodução:
- `elitismo`: O mehor individuo de cada geração tem seus "genes" compartilhados com o resto da população;
- `(pseudo) roleta`: Os 3 melhores individuos de cada geração disputam para terem seus "genes compartilhados com o resto da população"
- `aleatório`: Indivíduos aleatórios tem seus "genes" compartilhados com o resto da população;

Tanto o proprio Ag1 quanto os métodos de melhoria possuem parâmetros que podem ser alterados e impactam na velocidade e precisão do algorítmo.
    
## Ag2

Esse é o algoritmo genético responsável por encontrar os melhores parâmetros para treinar o *Ag1*.

Ele possui alguns parâmetros que são alterados evolutivamente:

- `mutacao`: Define a taxa de mutação empregada no *Ag1*;
- `pop_inicial`: Define a população inicial utilizada para o treino do *Ag1*;
- `metodo_selecao`: Define o método de seleção (reprodução) do *Ag1*;
- `qtd_predacao`: Define o número de indivíduos removidos no evento de predação.

Os valores são inicialmente escolhidos aleatoriamente e são alterados conforme o algoritmo progride iterativamente selecionando os valores que treinam o *Ag1* mais rapidamente.

A função de avaliação do *Ag2*, que pode ser observada no gráfico de *fitness* acima, utiliza a seguinte fórmula:

```
fitness = 2000 - n_geracoes
```

Em que `n_geracoes` é o número de iterações que o *Ag1* demorou para encontrar a melhor solução. No caso, queremos diminuir esse valor. Isso consequentemente aumentará o valor de *fitness*.

No quesito de reprodução, o algoritmo de seleção usado foi o *(pseudo) roleta*.

## Como executar o projeto

Para executar o programa, é necessário compilá-lo. Para isso, basta usar o comando `make`:

```
make ag2
```

Em seguida, será gerado o executável `ag2` que pode ser executado:

```
./ag2
```

## Vídeo de Apresentação

Um vídeo de explicação do projeto pode ser visto em:

https://youtu.be/5DmGCgxKAg0
