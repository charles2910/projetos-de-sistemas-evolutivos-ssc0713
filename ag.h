#ifndef AG
#define AG
#define MAX_X 500000
#define MAX_MUT 5
#define MAX_PRED 15
#define MAX_MET 1
#define MAX_POP 20



typedef struct {
	double mutacao;
	int pop_inicial;
	int geracoes;
	double threshold;
	int metodo_selecao;
	int qtd_predacao;
} gene;

typedef struct {
	int n_geracoes;
	double melhor;
} result_simulacao;

typedef struct melhor{
	double resultado;
	gene* gene_melhor;
}melhor;

result_simulacao * ag1(gene *);
void reproduz(double *, int *, gene *);
double altera_mutacao(double, double, double);
result_simulacao * gera_resultado(int, double);
double funcao(double);
int ranking(double *, int *, gene *);
int reproducao(double *, int *, gene *);
void elitismo(double *, int *, gene *);
void roleta(double *, int *, gene *);
void mutacao(double *, int *, gene *);
void predacao (double *, int *, gene *);

void print_gene(gene *);
double funcao2(gene*, result_simulacao*);
int ranking2(double *, int *);
void reproducao2(gene**, int *);
void elitismo2(gene** agente, int *rank);
void roleta2(gene** agente, int *rank);
void mutacao2(gene **agente, int *rank, double TaxMut);
void predacao2(gene** agentes, int* rank, int TaxPred);
gene* gera_gene2();

#endif
